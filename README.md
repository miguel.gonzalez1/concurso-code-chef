# Concurso Code Chef

El concurso de llama October Mega Cook-Off 2018 que se llevó acabo el día 21 de octubre del 2018 desde las 11:00 a.m. hasta las 1:30 p.m.

Este concurso de programación tenía dos categorías:

 * La división 1 era para los usuarios que tenían un puntaje superior a 1800
 * La división 2 era para los usuarios con menos de 1800 puntos

![screenshot](/images/screenshot.png)


La liga del concurso ya concluido es:


https://www.codechef.com/COOK99B?order=desc&sortBy=successful_submissions