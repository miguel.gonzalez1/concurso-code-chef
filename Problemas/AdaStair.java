package Problemas;

import java.util.Scanner;

public class AdaStair {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int T = Integer.parseInt(sc.nextLine());
        int N = 0;
        int K = 0;
        int [] h = {};        
        if (1 <= T && T <= 64){            
            for (int i = 0; i < T; i++) {
                N = sc.nextInt();
                int step = 0;
                if(1 <= N && N <= 128){
                    K = sc.nextInt();
                    h = new int[N];
                    if (1 <= K && K <= 1024) {
                        for(int i2 = 0; i2 < N; i2 ++){
                            h[i2] = sc.nextInt();
                        }
                    } else {
                        System.err.println("Invalid K number");
                    }
                } else{
                    System.err.println("Invalid N steps");
                }
                
                for(int index1 = 0; index1 < h.length; index1 ++){
                    int hi = h[index1];
                    int hf = 0;                    
                    boolean rightStep = true;          
                    if (index1 + 1 < h.length){
                        hf = h[index1 + 1];                        
                    } else {
                        rightStep = false;
                    }            
                    if(hf - hi <= K){
                        rightStep = true;
                    } else {
                        rightStep = false;
                    }
                    while (!rightStep){
                        hi = hi + K;
                        step++;
                        if(hf - hi <= K){
                            rightStep = true;
                        } else {
                            rightStep = false;
                        }
                    }            
                }        
                System.out.println(step);                
            }
           
        } else {
            System.err.println("Invalid test cases");
        }                
        
    }
}